.PHONY: help install gm

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Install the project
	composer install
	npm install
	npm run dev
	if [ ! -f "./.env" ];then \
		cp .env.example .env ; \
		php artisan key:generate ; \
	fi
	php artisan migrate

add-all: ## Add all changes to git.
	git add --all

commit: _phpcs ## Use this makefile to write a commit message
	git commit -m "$m"

push: test ## Push the changes to git
	git push $(options)

test: ## Check if phpunit passes.
	./vendor/bin/phpunit --coverage-text

_phpcs: ## Fix the phpcs errors if there are any.
	./vendor/bin/php-cs-fixer fix --config=.php_cs.dist -v $(options)